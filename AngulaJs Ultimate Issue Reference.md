#AngularJS Ultimate Issue Reference
--------


###Issue 1: Can one controller call another in AngularJS?

**Solution 1:** *[source][1]*

[1]: http://stackoverflow.com/questions/9293423/can-one-controller-call-another-in-angularjs

There are multiple ways how to communicate between controllers.

**The best one is probably sharing a service:**

```javascript
function FirstController(someDataService) {
  // use the data service, bind to template...
  // or call methods on someDataService to send a request to server
}
```
```javascript
function SecondController(someDataService) {
  // has a reference to the same instance of the service
  // so if the service updates state for example, this controller knows about it
}
```

**Another way is emitting an event on scope:**

```javascript
function FirstController($scope) {
  $scope.$on('someEvent', function() {});
  // another controller or even directive
}
```
```javascript
function SecondController($scope) {
  $scope.$emit('someEvent', args);
}
```

In both cases, you can communicate with any directive as well...


**Solution 2:** *[source][2]*

[2]:http://stackoverflow.com/a/16737459/1442566

Here is a way to call controller's function from outside of it: 
`angular.element(document.getElementById('yourControllerElementID')).scope().get();` `where get()` is a function from your controller.

you can switch `document.getElementById('yourControllerElementID')` to `$('#yourControllerElementID')` if you are using jQuery.

Also if your function means changing anything on your View, you should call `angular.element(document.getElementById('yourControllerElementID')).scope().$apply();` to apply the changes.

One more thing, you should note is that scopes are initialized after the page is loaded, so calling methods from outside of scope should always be done after the page is loaded. Else you will not get to the scope at all.

---

###Issue 2: Redirecting to a certain route based on condition?

**Solution 1:** *[source][1]*

[1]: http://stackoverflow.com/questions/11541695/angular-js-redirecting-to-a-certain-route-based-on-condition

I'm writing a small Angular.js app that has a login view and a main view, configured like so:

```
$routeProvider
 .when('/main' , {templateUrl: 'partials/main.html',  controller: MainController})
 .when('/login', {templateUrl: 'partials/login.html', controller: LoginController})
 .otherwise({redirectTo: '/login'});
```
My LoginController checks the user/pass combination and sets a property on the $rootScope reflecting this:

```
function LoginController($scope, $location, $rootScope) {
 $scope.attemptLogin = function() {
   if ( $scope.username == $scope.password ) { // test
        $rootScope.loggedUser = $scope.username;
        $location.path( "/main" );
    } else {
        $scope.loginError = "Invalid user/pass.";
    }
}
```
Everything works, but if I access `http://localhost/#/main` I end up bypassing the login screen. I wanted to write something like "whenever the route changes, if `$rootScope.loggedUser` is null then redirect to `/login`"

...

... wait. Can I listen to route changes somehow? I'll post this question anyway and keep looking.

**I added the following to my module configuration**

```
angular.module(...)
 .config( ['$routeProvider', function($routeProvider) {...}] )
 .run( function($rootScope, $location) {
    // register listener to watch route changes
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
      if ( $rootScope.loggedUser == null ) {
        // no logged user, we should be going to #login
        if ( next.templateUrl == "partials/login.html" ) {
          // already going to #login, no redirect needed
        } else {
          // not going to #login, we should redirect now
          $location.path( "/login" );
        }
      }         
    });
 })
```

The one thing that seems odd is that I had to test the partial name (login.html) because the "next" Route object did not have a url or something else. Maybe there's a better way?
